## Cypress Automated QA Tests

## Cypress
All needed information you can find here [cypress](https://www.cypress.io/). 
But generally speaking Cypress framework is a JavaScript-based end-to-end testing framework built on top of Mocha – a feature-rich JavaScript test framework running on and in the browser, making asynchronous testing simple and convenient. It also uses a BDD/TDD assertion library and a browser to pair with any JavaScript testing framework.

## Installation
Install nodejs and npm before running tests [NodeJS](https://nodejs.org/en/download/)

You don't need to do any pre-conditions to run tests. Just go through the following commands:

1. Install libs. Project directory is opened. In my case it looks like this C:\Users\Stanislav_Osikov\Downloads\simpleid>
```
   npm ci
```

2. To run all tests (including regression suite and e2e user scenario) perform:
```
   npx cypress run
```

3. To run smoke suite only:
```
   npx cypress run smoke
```

4. To generate Allure report:
```
   npm run allure:report
```

5. To open Allure report:
```
   npm run allure:open
```
Allure report will be the same as below. You can navigate between tests, look at the status codes, request bodies.

### Allure

![Allure report](allure.png)

### Scheme

![Cypress Automated QA Tests Diagram](cypress-automated-qa-tests-diagram.png)
