// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands';

// Alternatively you can use CommonJS syntax:
// require('./commands')
// eslint-disable-next-line import/no-extraneous-dependencies
import '@shelex/cypress-allure-plugin';
// eslint-disable-next-line import/no-extraneous-dependencies
import 'cypress-localstorage-commands';
// eslint-disable-next-line import/no-extraneous-dependencies
import '@bahmutov/cy-api/support';
// eslint-disable-next-line import/no-extraneous-dependencies
require('@cypress/skip-test/support');

const customCommands = require('./commands');

module.exports = {
  commands: customCommands,
};

before(() => {
  const allure = Cypress.Allure.reporter.getInterface();
  const env = Cypress.env();
  allure.writeEnvironmentInfo({
    Url: env.baseUrl,
    Environment: env.server,
  });

  allure.writeCategoriesDefinitions([
    {
      name: 'Skipped Tests',
    },
  ]);
});

beforeEach(() => {
  const allure = Cypress.Allure.reporter.getInterface();
  allure.feature('Actions Feature');
  allure.epic('Api tests');
  // eslint-disable-next-line no-unused-vars
  Cypress.on('uncaught:exception', (err, runnable) => {
    console.log('Cypress detected uncaught exception', err);
    return false;
  });
});
