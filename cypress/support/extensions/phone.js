import { randomId } from './randomId';

export const setPhoneNumber = () => cy.get('@token').then((val) => {
  cy.request({
    method: 'POST',
    url: '/consumer/registration/continue/setPhone',
    failOnStatusCode: false,
    headers: {
      Authorization: `Bearer ${val}`,
    },
    body: {
      data: {
        attributes: {
          phone: `+380667899${randomId}`,
        },
      },
    },
  }).then((response) => {
    expect(response.status)
      .eq(200);
  });
});

export const confirmOtpCode = () => cy.get('@token')
  .then((token) => {
    cy.get('@phoneCode')
      .then((val) => {
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.request({
          method: 'POST',
          url: '/consumer/registration/continue/phoneActivation',
          headers: {
            Authorization: `Bearer ${token}`,
          },
          failOnStatusCode: false,
          body: {
            data: {
              attributes: {
                code: Number(val),
              },
            },
          },
        })
          .then((response) => {
            expect(response.status)
              .eq(200);
          });
      });
  });
