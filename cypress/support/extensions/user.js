// eslint-disable-next-line import/prefer-default-export,import/no-cycle
import { randomId } from './randomId';

// eslint-disable-next-line import/no-mutable-exports,import/prefer-default-export
export const user = {
  email: `testuser++${randomId}@roket.us`,
  firstName: 'stas',
  lastName: 'osikov',
  token: '',
  emailCode: '',
  otpCode: '',
  phone: `+38095193${randomId}`,
};
