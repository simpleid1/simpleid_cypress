// @ts-check
/* eslint-env mocha */
import { user } from './user';

export const getToken1 = () => cy.request({
  method: 'POST',
  url: '/consumer/registration/start/setCredentials',
  body: {
    data: {
      attributes: {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        phone: user.phone,
        password: 'qwerty12345S@',
        agreement: true,
      },
    },
  },
}).then((response) => {
  expect(response.status).to.eq(200);
  expect(response.body).property('data').to.not.be.oneOf([null, '']);
  user.token = response.body.included[0].attributes.token;
  cy.wrap(response.body.included[0].attributes.token).as('token');
});

export const getConfirmationCodes1 = () => cy.get('@token')
  .then((val) => {
    cy.request({
      method: 'GET',
      url: '/consumer/getConfirmationCodes',
      headers: {
        Authorization: `Bearer ${val}`,
      },
    })
      .then((response) => {
        expect(response.status)
          .to
          .eq(200);
        user.emailCode = response.body.data[0].attributes.emailCode;
        user.emailCode = response.body.data[0].attributes.phoneCode;
        cy.wrap(response.body.data[0].attributes.emailCode)
          .as('emailCode');
        cy.log(`Email code${response.body}`);
        cy.log(response.body.data[0].attributes.emailCode);
        cy.wrap(response.body.data[0].attributes.phoneCode)
          .as('phoneCode');
      });
  });

export const validateEmail1 = () => cy.get('@emailCode').then((val) => {
  cy.request({
    method: 'GET',
    url: `/consumer/mail/validate/${val}`,
  }).then((response) => {
    expect(response.status).to.eq(204);
  });
});
