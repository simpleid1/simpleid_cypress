import { randomId } from './randomId';

// eslint-disable-next-line import/prefer-default-export
export const getCreditCard = () => cy.get('@token').then((val) => {
  cy.request({
    method: 'POST',
    url: '/ta/creditCard/create',
    headers: {
      Authorization: `Bearer ${val}`,
    },
    body: {
      data: {
        attributes: {
          number: `411111111111${randomId}`,
          expires: '11/2024',
          name: 'Stas Osikov',
          type: 'Visa',
        },
      },
    },
  }).then((response) => {
    expect(response.status).eq(204);
  });
});
