import { user } from './extensions/user';

Cypress.Commands.add('getAuthToken', () => {
  const id = Math.floor(Math.random() * 10000);
  const phone = Math.floor(Math.random() * 10000);
  cy.request({
    method: 'POST',
    url: '/consumer/registration/start/setCredentials',
    body: {
      data: {
        attributes: {
          firstName: user.firstName,
          lastName: user.lastName,
          email: `sosikov+${id}@roket.us`,
          phone: `+38095193${phone}`,
          password: 'qwerty12345S@',
          agreement: true,
        },
      },
    },
  }).then((response) => {
    expect(response.status).to.eq(200);
    expect(response.body).property('data').to.not.be.oneOf([null, '']);
    user.token = response.body.included[0].attributes.token;
    cy.wrap(response.body.included[0].attributes.token).as('token');
  });
});

Cypress.Commands.add('getConfirmationCodes', () => {
  cy.get('@token')
    .then((val) => {
      cy.request({
        method: 'GET',
        url: '/consumer/getConfirmationCodes',
        headers: {
          Authorization: `Bearer ${val}`,
        },
      })
        .then((response) => {
          expect(response.status)
            .to
            .eq(200);
          user.emailCode = response.body.data[0].attributes.emailCode;
          user.otpCode = response.body.data[0].attributes.phoneCode;
          cy.wrap(response.body.data[0].attributes.emailCode)
            .as('emailCode');
          cy.log(response.body.data[0].attributes.emailCode);
          cy.wrap(response.body.data[0].attributes.phoneCode)
            .as('phoneCode');
        });
    });
});

Cypress.Commands.add('validateEmail', () => {
  cy.get('@emailCode').then((val) => {
    cy.request({
      method: 'GET',
      url: `/consumer/mail/validate/${val}`,
    }).then((response) => {
      expect(response.status).to.eq(204);
    });
  });
});

Cypress.Commands.add('getCreditCard', () => {
  const card = Math.floor(Math.random() * 1000000);
  cy.get('@token').then((val) => {
    cy.request({
      method: 'POST',
      url: '/ta/creditCard/create',
      headers: {
        Authorization: `Bearer ${val}`,
      },
      body: {
        data: {
          attributes: {
            number: `4111111111${card}`,
            expires: '11/2024',
            name: 'Stas Osikov',
            type: 'Visa',
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(204);
    });
  });
});

Cypress.Commands.add('activatePhone', () => {
  cy.get('@token').then((token) => {
    cy.get('@phoneCode').then((val) => {
      // eslint-disable-next-line cypress/no-unnecessary-waiting
      cy.request({
        method: 'POST',
        url: '/consumer/registration/continue/phoneActivation',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        failOnStatusCode: false,
        body: {
          data: {
            attributes: {
              code: Number(val),
            },
          },
        },
      }).then((response) => {
        expect(response.status).eq(200);
        expect(response.body.data[0].attributes).to.have.property('nextStep', 'noMoreSteps');
        expect(response.body.included[0].attributes).to.have.property('phoneVerified', true);
      });
    });
  });
});
