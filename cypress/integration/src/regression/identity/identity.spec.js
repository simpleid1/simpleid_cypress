describe('Identity service', () => {
  it('Check that user is able to get my identity', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.get('@token').then((val) => {
      cy.request({
        method: 'GET',
        url: '/identities/my',
        headers: {
          Authorization: `Bearer ${val}`,
        },
      }).then((response) => {
        expect(response.status).eq(200);
      });
    });
  });

  it('Check that user is able to get my identity without token', () => {
    cy.request({
      method: 'GET',
      url: '/identities/my',
      failOnStatusCode: false,
      headers: {
        Authorization: 'Bearer ',
      },
    }).then((response) => {
      expect(response.status).eq(401);
    });
  });
});

afterEach('Clear local storage', () => {
  cy.clearLocalStorage();
});
