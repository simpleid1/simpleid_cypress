describe('Check resend phone code flow, negative way', () => {
  it('Check that user is not able to get resended otp code using incorrect phone number', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.validateEmail();
    cy.getCreditCard();
    cy.request({
      method: 'POST',
      url: '/consumer/password/sendResetCode',
      failOnStatusCode: false,
      body: {
        data: {
          attributes: {
            phone: '1',
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(422);
      expect(response.body).property('data').to.not.be.oneOf([null, '']);
    });
  });

  it('Check that user is not able to get resended otp code without phone', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.validateEmail();
    cy.getCreditCard();
    cy.request({
      method: 'POST',
      url: '/consumer/password/sendResetCode',
      failOnStatusCode: false,
      body: {
        data: {
          attributes: {
            phone: '',
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(422);
    });
  });

  it('Check that user is not able to get resended otp code using wrong format phone', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.validateEmail();
    cy.getCreditCard();
    cy.request({
      method: 'POST',
      url: '/consumer/password/sendResetCode',
      failOnStatusCode: false,
      body: {
        data: {
          attributes: {
            phone: '#$^&*(',
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(422);
    });
  });
});

afterEach('Clear local storage', () => {
  cy.clearLocalStorage();
});
