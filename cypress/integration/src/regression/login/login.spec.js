import { user } from '../../../../support/extensions/user';

describe('User login', () => {
  it('Check that user is not able to login without password', () => {
    cy.request({
      method: 'POST',
      url: 'consumer/login',
      failOnStatusCode: false,
      body: {
        data: {
          attributes: {
            credential: user.email,
            password: '',
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(422);
    });
  });

  it('Check that user is not able to login without email', () => {
    cy.request({
      method: 'POST',
      url: 'consumer/login',
      failOnStatusCode: false,
      body: {
        data: {
          attributes: {
            credential: '',
            password: 'qwerty12345S@',
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(502);
    });
  });

  it('Check that user is not able to login using empty body', () => {
    cy.request({
      method: 'POST',
      url: 'consumer/login',
      failOnStatusCode: false,
      body: {
        data: {
          attributes: {
            credential: '',
            password: '',
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(502);
    });
  });
});

afterEach('Clear local storage', () => {
  cy.clearLocalStorage();
});
