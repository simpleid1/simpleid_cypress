import { randomId } from '../../../../support/extensions/randomId';

describe('Add card flow', () => {
  it('Check that user is not able to add a credit card without expiration date', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.get('@token').then((val) => {
      cy.request({
        method: 'POST',
        url: '/ta/creditCard/create',
        failOnStatusCode: false,
        headers: {
          Authorization: `Bearer ${val}`,
        },
        body: {
          data: {
            attributes: {
              number: `411111111111${randomId}`,
              expires: '',
              name: 'Polina Osikov',
              type: 'Visa',
            },
          },
        },
      }).then((response) => {
        expect(response.status).eq(422);
      });
    });
  });

  it('Check that user is not able to add a credit card without card number', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.get('@token').then((val) => {
      cy.request({
        method: 'POST',
        url: '/ta/creditCard/create',
        failOnStatusCode: false,
        headers: {
          Authorization: `Bearer ${val}`,
        },
        body: {
          data: {
            attributes: {
              number: '',
              expires: '11/2024',
              name: 'Stas Osikov',
              type: 'Visa',
            },
          },
        },
      }).then((response) => {
        expect(response.status).eq(422);
      });
    });
  });

  it('Check that user is not able to add a credit card without card type', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.get('@token').then((val) => {
      cy.request({
        method: 'POST',
        url: '/ta/creditCard/create',
        failOnStatusCode: false,
        headers: {
          Authorization: `Bearer ${val}`,
        },
        body: {
          data: {
            attributes: {
              number: `411111111111${randomId}`,
              expires: '12/2024',
              name: 'Stas Osikov',
              type: '',
            },
          },
        },
      }).then((response) => {
        expect(response.status).eq(422);
      });
    });
  });

  it('Check that user is not able to add a credit card using date which is less then current date', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.get('@token').then((val) => {
      cy.request({
        method: 'POST',
        url: '/ta/creditCard/create',
        failOnStatusCode: false,
        headers: {
          Authorization: `Bearer ${val}`,
        },
        body: {
          data: {
            attributes: {
              number: `411111111111${randomId}`,
              expires: '12/2020',
              name: 'Stas Osikov',
              type: 'Visa',
            },
          },
        },
      }).then((response) => {
        expect(response.status).eq(422);
      });
    });
  });

  it('Check that user is not able to add a credit card using incorrect card type', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.get('@token').then((val) => {
      cy.request({
        method: 'POST',
        url: '/ta/creditCard/create',
        failOnStatusCode: false,
        headers: {
          Authorization: `Bearer ${val}`,
        },
        body: {
          data: {
            attributes: {
              number: `411111111111${randomId}`,
              expires: '12/2028',
              name: 'Stas Osikov',
              type: 'Kharkiv',
            },
          },
        },
      }).then((response) => {
        expect(response.status).eq(422);
      });
    });
  });

  describe('Setting up credit card for a newly created user', () => {
    it('Check that user is able to set its credit card', () => {
      cy.getAuthToken();
      cy.getConfirmationCodes();
      cy.validateEmail();
      cy.get('@token').then((val) => {
        cy.request({
          method: 'POST',
          url: '/ta/creditCard/create',
          headers: {
            Authorization: `Bearer ${val}`,
          },
          body: {
            data: {
              attributes: {
                number: `411111111111${randomId}`,
                expires: '11/2025',
                name: 'Stas Osikov',
                type: 'Visa',
              },
            },
          },
        }).then((response) => {
          expect(response.status).eq(204);
        });
      });
    });
  });
});
