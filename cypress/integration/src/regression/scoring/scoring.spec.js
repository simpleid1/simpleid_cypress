describe('Scoring service', () => {
  it('Check that user is able to get its scoring', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.validateEmail();
    cy.getCreditCard();
    cy.get('@token').then((val) => {
      cy.request({
        method: 'GET',
        url: '/scoring/my',
        headers: {
          Authorization: `Bearer ${val}`,
        },
      }).then((response) => {
        expect(response.status).eq(200);
        expect(response.body.data[0].attributes).to.have.property('score', 32);
      });
    });
  });

  it('Check that user is not able to get its scoring without token', () => {
    cy.request({
      method: 'GET',
      url: '/scoring/my',
      failOnStatusCode: false,
      headers: {
        Authorization: 'Bearer ',
      },
    }).then((response) => {
      expect(response.status).eq(401);
    });
  });
});

afterEach('Clear local storage', () => {
  cy.clearLocalStorage();
});
