describe('Change phone flow', () => {
  it('Check that user is not able to change phone using an phone field', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.get('@token').then((token) => {
      // eslint-disable-next-line cypress/no-unnecessary-waiting
      cy.request({
        method: 'POST',
        url: '/consumer/mail/change',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        failOnStatusCode: false,
        body: {
          data: {
            attributes: {
              email: '',
            },
          },
        },
      }).then((response) => {
        expect(response.status).to.eq(422);
        expect(response.body.data[0]).to.have.property('title', 'wrong email format');
        expect(response.body.data[0]).to.have.property('detail', 'wrong email format');
      });
    });
  });

  it('Check that user is not able to change email using wrong format email', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.get('@token').then((token) => {
      // eslint-disable-next-line cypress/no-unnecessary-waiting
      cy.request({
        method: 'POST',
        url: '/consumer/mail/change',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        failOnStatusCode: false,
        body: {
          data: {
            attributes: {
              email: 'test',
            },
          },
        },
      }).then((response) => {
        expect(response.status).to.eq(422);
        expect(response.body.data[0]).to.have.property('title', 'wrong email format');
        expect(response.body.data[0]).to.have.property('detail', 'wrong email format');
      });
    });
  });

  it('Check that user is not able to change email using wrong format email', () => {
    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.request({
      method: 'POST',
      url: '/consumer/mail/change',
      headers: {
        Authorization: 'Bearer ',
      },
      failOnStatusCode: false,
      body: {
        data: {
          attributes: {
            email: 'test',
          },
        },
      },
    }).then((response) => {
      expect(response.status).to.eq(401);
    });
  });
});

afterEach('Clear local storage', () => {
  cy.clearLocalStorage();
});
