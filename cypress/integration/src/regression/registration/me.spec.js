describe('Me service', () => {
  it('Check that user is able to get its current status', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.get('@token').then((val) => {
      cy.request({
        method: 'GET',
        url: '/consumer/me',
        headers: {
          Authorization: `Bearer ${val}`,
        },
      }).then((response) => {
        expect(response.status).eq(200);
        expect(response.body.data[0].attributes).to.have.property('emailVerified', false);
        expect(response.body.data[0].attributes).to.have.property('phoneVerified', false);
      });
    });
  });

  it('Check that user is not able to get its current status without token', () => {
    cy.request({
      method: 'GET',
      url: '/consumer/me',
      failOnStatusCode: false,
      headers: {
        Authorization: 'Bearer ',
      },
    }).then((response) => {
      expect(response.status).eq(401);
    });
  });

  it('Check that user is able to get its current status and confirm that email has been successfully validated', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.validateEmail();
    cy.get('@token').then((val) => {
      cy.request({
        method: 'GET',
        url: '/consumer/me',
        headers: {
          Authorization: `Bearer ${val}`,
        },
      }).then((response) => {
        expect(response.status).eq(200);
        expect(response.body.data[0].attributes).to.have.property('emailVerified', true);
        expect(response.body.data[0].attributes).to.have.property('phoneVerified', false);
      });
    });
  });
});
