import { randomId } from '../../../../support/extensions/randomId';
import { user } from '../../../../support/extensions/user';

describe('Phone verification', () => {
  describe('Setting up phone for a newly created user if some required data is missed', () => {
    it('Check that user is not able to get otp code without phone number', () => {
      cy.getAuthToken();
      cy.get('@token').then((val) => {
        cy.request({
          method: 'POST',
          url: '/consumer/registration/continue/setPhone',
          failOnStatusCode: false,
          headers: {
            Authorization: `Bearer ${val}`,
          },
          body: {
            data: {
              attributes: {
                phone: '',
              },
            },
          },
        }).then((response) => {
          expect(response.status).eq(422);
          expect(response.body).property('data').to.not.be.oneOf([null, '']);
        });
      });
    });

    it('Check that user is not able to proceed with an incorrect otp code', () => {
      cy.getAuthToken();
      cy.get('@token').then((val) => {
        cy.request({
          method: 'POST',
          url: '/consumer/registration/continue/setPhone',
          failOnStatusCode: false,
          headers: {
            Authorization: `Bearer ${val}`,
          },
          body: {
            data: {
              attributes: {
                code: `${randomId}`,
              },
            },
          },
        }).then((response) => {
          expect(response.status).eq(422);
          expect(response.body).property('data').to.not.be.oneOf([null, '']);
        });
      });
    });

    it('Check that user is not able to proceed with an empty otp code', () => {
      cy.getAuthToken();
      cy.get('@token').then((val) => {
        cy.request({
          method: 'POST',
          url: '/consumer/registration/continue/setPhone',
          failOnStatusCode: false,
          headers: {
            Authorization: `Bearer ${val}`,
          },
          body: {
            data: {
              attributes: {
                code: '',
              },
            },
          },
        }).then((response) => {
          expect(response.status).eq(422);
          expect(response.body).property('data').to.not.be.oneOf([null, '']);
        });
      });
    });

    it('Check that user is not able to change phone which is belong to another consumer', () => {
      cy.getAuthToken();
      cy.get('@token').then((val) => {
        cy.request({
          method: 'POST',
          url: '/consumer/registration/continue/setPhone',
          failOnStatusCode: false,
          headers: {
            Authorization: `Bearer ${val}`,
          },
          body: {
            data: {
              attributes: {
                phone: user.phone,
              },
            },
          },
        }).then((response) => {
          expect(response.status).eq(422);
          expect(response.body).property('data').to.not.be.oneOf([null, '']);
        });
      });
    });

    it('Check that user is not able to change phone if no phone is provided', () => {
      cy.getAuthToken();
      cy.get('@token').then((val) => {
        cy.request({
          method: 'POST',
          url: '/consumer/registration/continue/setPhone',
          failOnStatusCode: false,
          headers: {
            Authorization: `Bearer ${val}`,
          },
          body: {
            data: {
              attributes: {
                phone: '',
              },
            },
          },
        }).then((response) => {
          expect(response.status).eq(422);
        });
      });
    });

    it('Check that user is not able to change phone using special characters', () => {
      cy.getAuthToken();
      cy.get('@token').then((val) => {
        cy.request({
          method: 'POST',
          url: '/consumer/registration/continue/setPhone',
          failOnStatusCode: false,
          headers: {
            Authorization: `Bearer ${val}`,
          },
          body: {
            data: {
              attributes: {
                phone: '^&&**(()))_!@@#',
              },
            },
          },
        }).then((response) => {
          expect(response.status).eq(422);
        });
      });
    });

    it('Check that user is not able to get an otp code which is being resend without token', () => {
      cy.request({
        method: 'POST',
        url: '/consumer/phone/activation/resend',
        failOnStatusCode: false,
        headers: {
          Authorization: 'Bearer ',
        },
        body: {},
      }).then((response) => {
        expect(response.status).eq(401);
      });
    });
  });
});

afterEach('Clear local storage', () => {
  cy.clearLocalStorage();
});
