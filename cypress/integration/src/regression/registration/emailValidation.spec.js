describe('Email validation service', () => {
  it('Check that user is not able to validate email using incorrect hash', () => {
    cy.request({
      method: 'GET',
      url: '/consumer/mail/validate/3ae23ac1-dbac-4ccb-9035-e357faf4b261',
      failOnStatusCode: false,
    }).then((response) => {
      expect(response.status).eq(422);
      expect(response.body.data[0]).to.have.property('title', 'email validation code wrong or already used');
      expect(response.body.data[0]).to.have.property('detail', 'email validation code wrong or already used');
    });
  });
});
