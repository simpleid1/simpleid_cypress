const { user } = require('../../../../support/extensions/user');

describe('Registration flow scenario to activate a user email', () => {
  it('Verify email', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.allure()
      .startStep('Check that email cannot be validated without token');
    cy.validateEmail();
    cy.allure()
      .endStep();
  });

  it('Check that user is unable to proceed with incorrect password', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.allure().startStep('Check incorrect phone code response');
    cy.request({
      method: 'POST',
      url: '/consumer/registration/start/setEmail',
      failOnStatusCode: false,
      body: {
        data: {
          attributes: {
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            password: 'qwerty12345S',
            agreement: true,
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(422);
      expect(response.body).property('data').to.not.be.oneOf([null, '']);
    });
    cy.allure().endStep();
  });

  it('Check that user is unable to proceed without password', () => {
    cy.request({
      method: 'POST',
      url: '/consumer/registration/start/setEmail',
      failOnStatusCode: false,
      body: {
        data: {
          attributes: {
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            password: '',
            agreement: true,
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(422);
      expect(response.body).property('data').to.not.be.oneOf([null, '']);
    });
  });

  it('Check that user is unable to proceed without email', () => {
    cy.request({
      method: 'POST',
      url: '/consumer/registration/start/setEmail',
      failOnStatusCode: false,
      body: {
        data: {
          attributes: {
            firstName: user.firstName,
            lastName: user.lastName,
            email: '',
            password: 'qwerty12345S@',
            agreement: true,
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(422);
      expect(response.body).property('data').to.not.be.oneOf([null, '']);
    });
  });

  it('Check that user is unable to proceed without last name', () => {
    cy.request({
      method: 'POST',
      url: '/consumer/registration/start/setEmail',
      failOnStatusCode: false,
      body: {
        data: {
          attributes: {
            firstName: user.firstName,
            lastName: '',
            email: user.email,
            password: 'qwerty12345S@',
            agreement: true,
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(422);
      expect(response.body).property('data').to.not.be.oneOf([null, '']);
    });
  });

  it('Check that user is unable to proceed without first name', () => {
    cy.request({
      method: 'POST',
      url: '/consumer/registration/start/setEmail',
      failOnStatusCode: false,
      body: {
        data: {
          attributes: {
            firstName: '',
            lastName: user.lastName,
            email: user.email,
            password: 'qwerty12345S@',
            agreement: true,
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(422);
      expect(response.body).property('data').to.not.be.oneOf([null, '']);
    });

    it('Check that user is unable to proceed using agreement as string', () => {
      cy.request({
        method: 'POST',
        url: '/consumer/registration/start/setEmail',
        failOnStatusCode: false,
        body: {
          data: {
            attributes: {
              firstName: user.firstName,
              lastName: user.lastName,
              email: user.email,
              password: 'qwerty12345S@',
              agreement: 'test',
            },
          },
        },
      }).then((response) => {
        expect(response.status).eq(422);
        expect(response.body).property('data').to.not.be.oneOf([null, '']);
      });
    });
  });
});

afterEach(() => {
  cy.clearLocalStorage();
  cy.clearLocalStorageSnapshot();
  cy.clearCookies();
});
