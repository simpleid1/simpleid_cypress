describe('Setting up phone for a newly created user', () => {
  it('Check that user is able to set its phone number using otp code', () => {
    cy.getAuthToken();
    cy.getConfirmationCodes();
    cy.validateEmail();
    cy.get('@token').then((token) => {
      cy.get('@phoneCode').then((val) => {
        // eslint-disable-next-line cypress/no-unnecessary-waiting
        cy.request({
          method: 'POST',
          url: '/consumer/registration/continue/phoneActivation',
          headers: {
            Authorization: `Bearer ${token}`,
          },
          failOnStatusCode: false,
          body: {
            data: {
              attributes: {
                code: Number(val),
              },
            },
          },
        }).then((response) => {
          expect(response.status).eq(200);
          expect(response.body.data[0].attributes).to.have.property('completed', true);
          expect(response.body.data[0].attributes).to.have.property('nextStep', 'noMoreSteps');
          expect(response.body.included[0].attributes).to.have.property('emailVerified', true);
          expect(response.body.included[0].attributes).to.have.property('phoneVerified', true);
        });
      });
    });
  });
});
