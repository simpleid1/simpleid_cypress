import { randomId } from '../../../../support/extensions/randomId';

const random = Cypress._.random(0, 10000);

const newEmail = `sosikov+${random}@roket.us`;

const registerUser = {
  email: `testuser+${randomId}@roket.us`,
  firstName: 'stas',
  lastName: 'osikov',
  password: 'qwerty12345S@',
  token: '',
  emailCode: '',
  otpCode: '',
  phone: `+38095193${randomId}`,
  creditCard: `4111111111${randomId}`,
  expirationDate: '11/2026',
  companyName: `Test Company ${randomId}`,
  companyJwt: '',
  requestId: '',
  requestIdByEmail: '',
};

describe('End to end user scenario to make sure that critical top user journey works as expected', () => {
  it('Register new consumer', () => {
    cy.request({
      method: 'POST',
      url: '/consumer/registration/start/setCredentials',
      body: {
        data: {
          attributes: {
            firstName: registerUser.firstName,
            lastName: registerUser.lastName,
            email: registerUser.email,
            phone: registerUser.phone,
            password: registerUser.password,
            agreement: true,
          },
        },
      },
    })
      .then((response) => {
        expect(response.status)
          .to
          .eq(200);
        expect(response.body)
          .property('data')
          .to
          .not
          .be
          .oneOf([null, '']);
        registerUser.token = response.body.included[0].attributes.token;
      });
  });

  it('Get confirmation codes', () => {
    cy.request({
      method: 'GET',
      url: '/consumer/getConfirmationCodes',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
    })
      .then((response) => {
        expect(response.status)
          .to
          .eq(200);
        registerUser.emailCode = response.body.data[0].attributes.emailCode;
        registerUser.otpCode = response.body.data[0].attributes.phoneCode;
        cy.wrap(response.body.data[0].attributes.emailCode)
          .as('emailCode');
        cy.log(response.body.data[0].attributes.emailCode);
        cy.wrap(response.body.data[0].attributes.phoneCode)
          .as('phoneCode');
      });
  });

  it('Activate phone', () => {
    cy.request({
      method: 'POST',
      url: '/consumer/registration/continue/phoneActivation',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
      failOnStatusCode: false,
      body: {
        data: {
          attributes: {
            code: Number(registerUser.otpCode),
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(200);
      expect(response.body.data[0].attributes).to.have.property('nextStep', 'noMoreSteps');
      expect(response.body.included[0].attributes).to.have.property('phoneVerified', true);
    });
  });

  it('Check me service', () => {
    cy.request({
      method: 'GET',
      url: '/consumer/me',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
    }).then((response) => {
      expect(response.status).eq(200);
      expect(response.body.data[0].attributes).to.have.property('emailVerified', false);
      expect(response.body.data[0].attributes).to.have.property('phoneVerified', true);
    });
  });

  it('Check scoring service 10', () => {
    cy.request({
      method: 'GET',
      url: '/scoring/my',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
    }).then((response) => {
      expect(response.status).eq(200);
      expect(response.body.data[0].attributes).to.have.property('score', 10);
    });
  });

  it('Activate email', () => {
    cy.request({
      method: 'GET',
      url: `/consumer/mail/validate/${registerUser.emailCode}`,
    }).then((response) => {
      expect(response.status).to.eq(204);
    });
  });

  it('Check me service', () => {
    cy.request({
      method: 'GET',
      url: '/consumer/me',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
    }).then((response) => {
      expect(response.status).eq(200);
      expect(response.body.data[0].attributes).to.have.property('emailVerified', true);
      expect(response.body.data[0].attributes).to.have.property('phoneVerified', true);
    });
  });

  it('Check scoring service 12', () => {
    cy.request({
      method: 'GET',
      url: '/scoring/my',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
    }).then((response) => {
      expect(response.status).eq(200);
      expect(response.body.data[0].attributes).to.have.property('score', 12);
    });
  });

  it('Add card', () => {
    cy.request({
      method: 'POST',
      url: '/ta/creditCard/create',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
      body: {
        data: {
          attributes: {
            number: registerUser.creditCard,
            expires: registerUser.expirationDate,
            name: 'Stas Osikov',
            type: 'Visa',
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(204);
    });
  });

  it('Check scoring 42', () => {
    cy.request({
      method: 'GET',
      url: '/scoring/my',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
    }).then((response) => {
      expect(response.status).eq(200);
      expect(response.body.data[0].attributes).to.have.property('score', 42);
    });
  });

  it('Check my identities service 3 identities', () => {
    cy.request({
      method: 'GET',
      url: '/identities/my',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
    }).then((response) => {
      expect(response.status).eq(200);
      // cy.get(response.body).should('have.length', 3);
    });
  });

  it('Check active requests', () => {
    cy.request({
      method: 'GET',
      url: '/identities/my',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
    }).then((response) => {
      expect(response.status).eq(200);
      // cy.get(response.body).should('have.length', 3);
    });
  });

  it('Check history', () => {
    cy.request({
      method: 'GET',
      url: '/scoring/history',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
    }).then((response) => {
      expect(response.status).eq(200);
      // cy.get(response.body).should('have.length', 3);
    });
  });

  it('Register new customer', () => {
    cy.request({
      method: 'POST',
      url: '/customer/register',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
      body: {
        data: {
          attributes: {
            name: registerUser.companyName,
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(200);
      registerUser.companyJwt = response.body.data[0].attributes.jwt;
    });
  });

  it('Check requests tracking', () => {
    cy.request({
      method: 'GET',
      url: '/customer/score/history',
      headers: {
        Authorization: `Bearer ${registerUser.companyJwt}`,
      },
    }).then((response) => {
      expect(response.status).eq(200);
      // cy.get(response.body).should('have.length', 3);
    });
  });

  it('Send request for consumer', () => {
    cy.request({
      method: 'POST',
      url: '/customer/score/requestByIdentities',
      failOnStatusCode: false,
      headers: {
        Authorization: `Bearer ${registerUser.companyJwt}`,
      },
      body: {
        data: {
          attributes: {
            metadata: {
              orderID: 'test',
              Some: '5433',
            },
            description: 'Transaction description',
            identities: [
              {
                type: 'email',
                payload: {
                  email: registerUser.email,
                },
              },
              {
                type: 'phone',
                payload: {
                  phone: registerUser.phone,
                },
              },
              {
                type: 'creditCard',
                payload: {
                  number: registerUser.creditCard,
                  expires: registerUser.expirationDate,
                  name: 'Stas Osikov',
                  type: 'Visa',
                },
              },
            ],
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(422);
      registerUser.requestId = response.body.data[0].id;
      cy.log(registerUser.requestId);
    });
  });

  it('Check requests tracking', () => {
    cy.request({
      method: 'GET',
      url: '/customer/score/history',
      headers: {
        Authorization: `Bearer ${registerUser.companyJwt}`,
      },
    }).then((response) => {
      expect(response.status).eq(200);
    });
  });

  it('Customer transaction tracking', () => {

  });

  it('Send second request for consumer by email', () => {
    cy.request({
      method: 'POST',
      url: '/customer/score/requestByIdentities',
      failOnStatusCode: false,
      headers: {
        Authorization: `Bearer ${registerUser.companyJwt}`,
      },
      body: {
        data: {
          data: {
            attributes: {
              public_metadata: {
                amount: '$233',
                location: 'location',
                desc: 'some descr',
                ccDigits: '5454',
              },
              private_metadata: {
                orderID: '123412341234',
                clientID: '9876987',
              },
              identities: [
                {
                  type: 'email',
                  payload: {
                    email: registerUser.email,
                  },
                },
              ],
            },
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(422);
      registerUser.requestIdByEmail = response.body.data[0].id;
      cy.log(registerUser.requestIdByEmail);
    });
  });

  it('Accept request', () => {
    cy.request({
      method: 'PUT',
      url: `/scoring/acceptRequest/${registerUser.requestId}`,
      failOnStatusCode: false,
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
    }).then((response) => {
      expect(response.status).eq(422);
    });
  });

  it('Decline request', () => {
    cy.request({
      method: 'PUT',
      url: `/scoring/declineRequest/${registerUser.requestIdByEmail}`,
      failOnStatusCode: false,
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
    }).then((response) => {
      expect(response.status).eq(422);
    });
  });

  it('Check requests tracking', () => {
    cy.request({
      method: 'GET',
      url: '/customer/score/history',
      headers: {
        Authorization: `Bearer ${registerUser.companyJwt}`,
      },
    }).then((response) => {
      expect(response.status).eq(200);
      // cy.get(response.body).should('have.length', 3);
    });
  });

  it('Customer request tracking', () => {

  });

  it.skip('Change consumer email', () => {
    cy.request({
      method: 'POST',
      url: '/consumer/mail/change',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
      body: {
        data: {
          attributes: {
            email: `${newEmail}`,
          },
        },
      },
    }).then((response) => {
      expect(response.status).to.eq(200);
    });
  });

  it('Check me service', () => {
    cy.request({
      method: 'GET',
      url: '/consumer/me',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
    }).then((response) => {
      expect(response.status).eq(200);
      expect(response.body.data[0].attributes).to.have.property('emailVerified', true);
      expect(response.body.data[0].attributes).to.have.property('phoneVerified', true);
    });
  });

  it('Customer request by new email', () => {
    cy.request({
      method: 'POST',
      url: '/customer/score/requestByIdentities',
      failOnStatusCode: false,
      headers: {
        Authorization: `Bearer ${registerUser.companyJwt}`,
      },
      body: {
        data: {
          attributes: {
            public_metadata: {
              amount: '$233',
              location: 'location',
              desc: 'some descr',
              ccDigits: '5454',
            },
            private_metadata: {
              orderID: '123412341234',
              clientID: '9876987',
            },
            identities: [
              {
                type: 'email',
                payload: {
                  email: registerUser.email,
                },
              },
            ],
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(422);
      registerUser.requestIdByEmail = response.body.data[0].id;
      cy.log(registerUser.requestIdByEmail);
    });
  });

  it('Get active requests', () => {
    cy.request({
      method: 'GET',
      url: '/scoring/getActiveRequests',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
    }).then((response) => {
      expect(response.status).eq(200);
      // cy.get(response.body).should('have.length', 3);
    });
  });

  it('Login by a new email', () => {
    cy.request({
      method: 'POST',
      url: 'consumer/login',
      body: {
        data: {
          attributes: {
            credential: registerUser.email,
            password: registerUser.password,
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(200);
    });
  });

  it('Login with old email', () => {
    cy.request({
      method: 'POST',
      url: 'consumer/login',
      failOnStatusCode: false,
      body: {
        data: {
          attributes: {
            credential: registerUser.email,
            password: newEmail,
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(422);
    });
  });

  it('Check history of requests', () => {
    cy.request({
      method: 'GET',
      url: '/scoring/history',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
    }).then((response) => {
      expect(response.status).eq(200);
      // cy.get(response.body).should('have.length', 3);
    });
  });

  it('Change phone', () => {
    cy.request({
      method: 'POST',
      url: '/consumer/phone/change',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
      failOnStatusCode: false,
      body: {
        data: {
          attributes: {
            phone: `+3809912345${random}`,
          },
        },
      },
    }).then((response) => {
      expect(response.status).to.eq(204);
    });
  });

  it('Reset password', () => {
    cy.request({
      method: 'POST',
      url: '/consumer/password/sendResetCode',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
      body: {
        data: {
          attributes: {
            phone: registerUser.phone,
          },
        },
      },
    }).then((response) => {
      expect(response.status).to.eq(204);
    });
  });

  it('Login', () => {
    cy.request({
      method: 'POST',
      url: 'consumer/login',
      body: {
        data: {
          attributes: {
            credential: registerUser.email,
            password: registerUser.password,
          },
        },
      },
    }).then((response) => {
      expect(response.status).eq(200);
    });
  });

  it('Check me service', () => {
    cy.request({
      method: 'GET',
      url: '/consumer/me',
      headers: {
        Authorization: `Bearer ${registerUser.token}`,
      },
    }).then((response) => {
      expect(response.status).eq(200);
      expect(response.body.data[0].attributes).to.have.property('emailVerified', true);
      expect(response.body.data[0].attributes).to.have.property('phoneVerified', true);
    });
  });
});
